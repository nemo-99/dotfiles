# Linux Ricing Dotfiles

+ The various folders contain the configuration files for all the pertinent applications used.
+ I have done my best to include links for documentation and installation instructions to make the process of switching to my rice and further customizing as seemless as possible

## Screenshots


![Rofi screenshot](./screenshots/rofi_screenshot.png)

![Screenshot1](./screenshots/screenshot_1.png)

![Screenshot2](./screenshots/screenshot_2.png)

## Alacritty

+ It is a GPU accelerated terminal emulator that is easy to customize
+ Its various dependencies and the different methods to install them are mentioned here:<br>
[Alacritty Github Dependency Page]( https://github.com/alacritty/alacritty/blob/master/INSTALL.md#debianubuntu) <br>
[Alacritty Release Page]( https://github.com/alacritty/alacritty/releases)
## Dunst

+ It is a light-weight and easily customizable notification daemon
+ Dependencies: <br>
[Dunst Dependencies page]( https://github.com/dunst-project/dunst/wiki/Dependencies) <br>
[Dunst Releases Page]( https://github.com/dunst-project/dunst/releases)
## i3

+ i3 is a window tiling manager
+ I've specifically used the i3-gaps fork of i3 which has several aesthetic benefits over the original i3 window manager

    [Release Page]( https://github.com/Airblader/i3/releases)

### i3lock-color

+ i3lock-color is a customizable lock screen for i3

    [Github Page]( https://github.com/Raymo111/i3lock-color)

## Picom

+ Picom is a compositor fork of compton
+ I've used a fork of Picom by ibhagwan which has several nifty features such as rounded corners for windows and kawase blurring of windows
+ Both of the aforementioned features are GPU accelerated

    [Picom github page]( https://github.com/ibhagwan/picom)

## Polybar

+ It is a heavily customizable status bar

    [Polybar github page]( https://github.com/polybar/polybar)

## Rofi 

+ Rofi is a customizable window switcher and application launcher

    [Rofi Github Page]( https://github.com/davatorium/rofi)

## Vim

+ It is highly customizable text editor
+ While it may have a learning curve, can increase productivity once you get used to it
+ Has tons of plugins available for use to enhance usability and transform it into a full-fledged IDE
+ Type `vimtutor` on your terminal to get a quick tutorial to ease into it

