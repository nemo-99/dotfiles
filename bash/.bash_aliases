alias python=python3
alias v="vi"
alias gs="git status"
alias gc="git commit"
alias spd="speedtest"
alias bfg="java -jar ~/bfg-1.13.0.jar"
alias c="clear"
